#!/bin/bash

REPO=almass-build
TAG=0.4
IMAGE="$REPO:$TAG"
DOCKERFILE="Dockerfile.llvm13.build"

almass_all_dir="$(dirname "$(pwd)")/almass_all"
echo $almass_all_dir

if [[ -z "$(docker images -q $IMAGE 2> /dev/null)" ]]; then
  docker build -f $DOCKERFILE -t $IMAGE .
fi

docker run                      \
    --mount type=bind,source=$almass_all_dir,target=/src/almass_all \
    --workdir /src/almass_all   \
    -it $IMAGE                  \
    /bin/bash -c "/src/scripts/build-almass.sh"

