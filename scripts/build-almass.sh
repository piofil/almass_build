#!/bin/bash

cmake . -D CMAKE_BUILD_TYPE=RELEASE -DCMAKE_CXX_COMPILER=/usr/local/bin/clang++ -DCMAKE_C_COMPILER=/usr/local/bin/clang -DCMAKE_CXX_FLAGS=-v
cmake --build . --parallel 4